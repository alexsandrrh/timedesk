global.fs = require('fs');
global.express = require('express');
global.app = express();

app.use('/assets', express.static(__dirname + '/public/assets'));

// static pages
app.get('/',function (req, res) {
    res.render(__dirname + '/public/account/desk.ejs');
});

app.get('/desk', function (req, res) {
   res.render(__dirname + '/public/account/desk.ejs');
});

app.get('/library', function (req, res) {
    res.render(__dirname + '/public/account/library.ejs');
});

app.get('/promo', function (req, res) {
    res.render(__dirname + '/public/account/promo.ejs');
});

app.listen(3000, console.log('Server is working\n' + 'Tracking port: 3000\n'));