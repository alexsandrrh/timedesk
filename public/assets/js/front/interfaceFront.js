let interfaceFront = {};

interfaceFront.items_func = [
    {
        "img_func" : "https://marvelapp.com/static/apiTeam@2x-5a72bbd0df3f17c4851dca773b2b1b8f-b4488.png",
        "headline_func" : "Выделенная команда API",
        "content_func" : "Наша команда может ответить на вопросы через наше Сообщество Slack Developer и электронную почту."
    },
    {
        "img_func" : "https://marvelapp.com/static/graphQL@2x-b69e75dd6c6ca2be4ac78829e1f89283-b4488.png",
        "headline_func" : "Я саша",
        "content_func" : "Предоставление вам гибкости для выбора всех необходимых данных с помощью одного запроса."
    },
    {
        "img_func" : "https://marvelapp.com/static/superchargeWorkflow@2x-c590dc9675fc826caa7e716cf8729c90-e871c.png",
        "headline_func" : "Перезагрузите рабочий процесс",
        "content_func" : "Автоматизируйте рабочие процессы, извлекайте проекты и анализируйте данные, чтобы перенести Slack на следующий уровень."
    }
];

interfaceFront.appendItems_func = function (data, parent) {
    for (let i = 0; i < data.length; i++) {
        $(parent).append(`
                    <div class="section-func__item">
                        <div class="item-illustration">
                            <img src="${data[i].img_func}" class="img">
                        </div>
                        <div class="item-content">
                            <h3>${data[i].headline_func}</h3>
                            <p>${data[i].content_func}</p>
                        </div>
                    </div>`);
    }
};
