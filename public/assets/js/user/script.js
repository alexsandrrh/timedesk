$(document).ready(function () {
    let mainObj = {};

    mainObj.btnHeader = {
        btnMenuOpen : $('.js-btnOpenMenu'),
        btnMenuClose : $('.js-btnCloseMenu'),
        btnToggleNotify : $('.js-btnToggleNotify')
    };


    mainObj.userData = {
        avatar : "https://yapic.yandex.ru/get/97189727/islands-retina-middle",
        name : "Aleksandr",
        surname : "Sadov",
        nickname : "Alexsandrrh"
    };

    mainObj.menuData = [
        {
            'link_name' : 'Главная',
            'link_path' : '/',
            'link_class' : 'main'
        },
        {
            'link_name' : 'Библиотека',
            'link_path' : '/library',
            'link_class' : 'library'
        },
        {
            'link_name' : 'Подарки и бонусы',
            'link_path' : '/promo',
            'link_class' : 'promo'
        },
        {
            'link_name' : 'Помощь',
            'link_path' : '/support',
            'link_class' : 'support'
        }
    ];

    interfaceHeader.showSessionHeadline('ru_RU');
    interfaceHeader.checkUserAvatar(mainObj.userData);
    interfaceHeader.preventMenu(mainObj.menuData);

    mainObj.btnHeader.btnMenuOpen.on('click', function () {
        $('.header-mobile__menu').addClass('-open');
    });

    mainObj.btnHeader.btnMenuClose.on('click', function () {
        $('.header-mobile__menu').removeClass('-open');
    });

    mainObj.btnHeader.btnToggleNotify.on('click', function () {
        $('.header-mobile__other').toggleClass('-notify');
    });





});