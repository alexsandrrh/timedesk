let interfaceHeader = {};


interfaceHeader.interFaces = {
    noneNotify : ''
};

interfaceHeader.headlines = [
    {
        path_hl : '/',
        name_hl : {
            ru_RU : 'Главная',
            en_EN : 'Main'
        }
    },
    {
        path_hl : '/library',
        name_hl : {
            ru_RU : 'Библиотека',
            en_EN : 'Library'
        }
    },
    {
        path_hl : '/promo',
        name_hl : {
            ru_RU : 'Подарки и бонусы',
            en_EN : 'Promo'
        }
    }
];

interfaceHeader.showSessionHeadline = function (lang) {
    interfaceHeader.headlines.forEach(function (page) {
        if (page.path_hl === location.pathname) {
            $('.js-headlinePage').html(page.name_hl[lang]);
        }
    });
};

interfaceHeader.renderAvatar = function (name, surname) {
    return `<div class="avatar avatar-static">
                        <p>${name[0]}${surname[0]}</p>
                    </div>`;

};

interfaceHeader.checkUserAvatar = function (data) {
    if (data.avatar === '' || data.avatar === null) {
        $('.js-btnUserAvatar').html(interfaceHeader.renderAvatar(data.name, data.surname));
    } else {
        $('.js-btnUserAvatar').html(`<img class="avatar" src="${data.avatar}">`);
    }
};

interfaceHeader.checkSessionMenu = function (item) {
    if (location.pathname === item.link_path) {
        if (item.link_class !== '') {
            $('.' + item.link_class + '').addClass('-this');
        }
    }
};

interfaceHeader.preventMenu = function (data) {
    data.forEach(function (nav) {
        $('.js-listMenuNav').append(`<a class="btn-nav ${nav.link_class}" href="${nav.link_path}">${nav.link_name}</a>`);
        interfaceHeader.checkSessionMenu(nav);
    });
};

interfaceHeader.checkNotify = function (data) {
    if (data === undefined) {
        $('.js-listNotify').html();
    } else {

    }
};





