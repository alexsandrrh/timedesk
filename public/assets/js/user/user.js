$(document).ready(function () {
    // object account
    let objUser = {};

    let bodyAction = $('.action-body');
    objUser.userParent = $('.other-blocks');

    // btn account
    objUser.btnOpen = $('.js-btnUserPageOpen');
    objUser.btnClose = $('.js-btnUserPageClose');

    // showNotify
    objUser.btnOpen.on('click', function () {
        objUser.userParent.addClass('-account');
        bodyAction.addClass('-active');
    });

   objUser.btnClose.on('click', function () {
        objUser.userParent.removeClass('-account');
        bodyAction.removeClass('-active');
    });
});