$(document).ready(function () {
    // object notify
    let objNotify = {};

    let bodyAction = $('.action-body');
    objNotify.notifyParent = $('.other-blocks');

    // notify content
    objNotify.ContentNotify = $('.js-blockContentNotify');

    objNotify.btnOpen = $('.js-btnNotifyOpen');
    objNotify.btnClose = $('.js-btnNotifyClose');

    // btn category notify
    objNotify.btnNew = $('.js-btnNotifyNew');
    objNotify.btnLooked = $('.js-btnNotifyLooked');


    // showNotify
    objNotify.btnOpen.on('click', function () {
        objNotify.notifyParent.addClass('-notify');
        bodyAction.addClass('-active');
    });

    objNotify.btnClose.on('click', function () {
        objNotify.notifyParent.removeClass('-notify');
        bodyAction.removeClass('-active');
    });

    // Api Started Notify
    objNotify.getCardNotify = function  (a, message) {
        return '<div class="card-notify" data-user_id="' + message[a].author_id + '">\n' +
            '                        <div class="card-notify__header">\n' +
            '                            <div class="notify-header__info">\n' +
            '                                <img class="account-photo" src="' + message[a].author_photo + '" alt="">\n' +
            '                                <p class="account-nickname">' + message[a].author + '</p>\n' +
            '                                <p class="account-summary__nickname">@' + message[a].author+ '</p>\n' +
            '                            </div>\n' +
            '                            <div class="card-notify__type"></div>\n' +
            '                        </div>\n' +
            '                        <div class="card-notify__content">\n' + message[a].content_code + '</div>\n' +
            '                    </div>';
    };

    objNotify.showNoneNotify = function () {
        return '<div class="notify-none">\n' +
            '                        <img src="https://marvelapp.com/static/illustration@2x-f97a6fa109c9b7db0c85968e4c078817-38548.jpg">\n' +
            '                        <div class="notify-none__content">\n' +
            '                            <h2>Ой! Вам ещё не приходили уведомления</h2>\n' +
            '                            <p>При появлении какой-то необходимой для вас информации, мы вас оповестим.</p>\n' +
            '                        </div>\n' +
            '                    </div>';
    };

    objNotify.checkNotify = function (data) {
        if (data.length === 0) {
            objNotify.ContentNotify.html(objNotify.showNoneNotify());
        } else {
            data.forEach(function (i) {
                objNotify.ContentNotify.html(getCardNotify(i, data));
            });
        }
    };

    // get first notify session
    $.getJSON(`${location.protocol}/api/notify/new`, function (dataNotify) {
        objNotify.checkNotify(dataNotify);
    });

    // changeMenu
    // Looked
    objNotify.btnLooked.on('click', function () {
        $(this).addClass('-this');
        objNotify.btnNew.removeClass('-this');
        $.getJSON(`${location.protocol}/api/notify/looked`, function (dataNotify) {
            objNotify.checkNotify(dataNotify);
        });
    });

    // New
    objNotify.btnNew.on('click', function () {
        $(this).addClass('-this');
        objNotify.btnLooked.removeClass('-this');
        $.getJSON(`${location.protocol}/api/notify/new`, function (dataNotify) {
            objNotify.checkNotify(dataNotify);
        });
    });
});